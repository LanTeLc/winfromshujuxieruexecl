﻿Imports System.Runtime.InteropServices

Public Class Common

    <DllImport("mwrf32.dll", EntryPoint:="rf_init", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_init(ByVal port As Int16, ByVal baud As Integer) As Integer
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_exit", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_exit(ByVal icdev As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_beep", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_beep(ByVal icdev As Integer, ByVal msec As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_get_status", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_get_status(ByVal icdev As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal status() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_load_key", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_load_key(ByVal icdev As Integer, ByVal mode As Integer, ByVal secnr As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal keybuff() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_load_key_hex", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_load_key_hex(ByVal icdev As Integer, ByVal mode As Integer, ByVal secnr As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal keybuff() As Byte) As Int16
    End Function
    <DllImport("mwrf32.dll", EntryPoint:="a_hex", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function a_hex(<MarshalAs(UnmanagedType.LPArray)> ByVal asc() As Byte, <MarshalAs(UnmanagedType.LPArray)> ByVal hex() As Byte, ByVal len As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="hex_a", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function hex_a(<MarshalAs(UnmanagedType.LPArray)> ByVal hex() As Byte, <MarshalAs(UnmanagedType.LPArray)> ByVal asc() As Byte, ByVal len As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_reset", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_reset(ByVal icdev As Integer, ByVal msec As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_set_control_bit", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_set_control_bit(ByVal icdev As Integer, ByVal _b As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_clr_control_bit", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_clr_control_bit(ByVal icdev As Integer, ByVal _b As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_disp8", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_disp8(ByVal icdev As Integer, ByVal mode As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal disp() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_disp", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_disp(ByVal icdev As Integer, ByVal mode As Integer, ByVal digit As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_encrypt", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_encrypt(<MarshalAs(UnmanagedType.LPArray)> ByVal key() As Byte, <MarshalAs(UnmanagedType.LPArray)> ByVal ptrsource() As Byte, ByVal len As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal ptrdest() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_decrypt", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_decrypt(<MarshalAs(UnmanagedType.LPArray)> ByVal key() As Byte, <MarshalAs(UnmanagedType.LPArray)> ByVal ptrsource() As Byte, ByVal len As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal ptrdest() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_srd_eeprom", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_srd_eeprom(ByVal icdev As Integer, ByVal offset As Integer, ByVal len As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal databuff() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_swr_eeprom", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_swr_eeprom(ByVal icdev As Integer, ByVal offset As Integer, ByVal len As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal databuff() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_setport", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_setport(ByVal icdev As Integer, ByVal _byte As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_getport", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_getport(ByVal icdev As Integer, ByRef _byte As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_gettime", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_gettime(ByVal icdev As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal time() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_gettime_hex", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_gettime_hex(ByVal icdev As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal time() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_settime", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_settime(ByVal icdev As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal time() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_settime_hex", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_settime_hex(ByVal icdev As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal time() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_setbright", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_setbright(ByVal icdev As Integer, ByVal bright As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_ctl_mode", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_ctl_mode(ByVal icdev As Integer, ByVal mode As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_disp_mode", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_disp_mode(ByVal icdev As Integer, ByVal mode As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="lib_ver", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function lib_ver(<MarshalAs(UnmanagedType.LPArray)> ByVal ver() As Byte) As Int16
    End Function
    ''' <summary>
    ''' 获取序列号
    ''' </summary>
    ''' <param name="icdev"></param>
    ''' <param name="msec"></param>
    ''' <param name="ver"></param>
    ''' <returns></returns>
    <DllImport("mwrf32.dll", EntryPoint:="rf_srd_snr", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_srd_snr(ByVal icdev As Integer, ByVal msec As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal ver() As Byte) As Int16

    End Function




End Class
