﻿Imports System.Runtime.InteropServices

Public Class mifareone

    <DllImport("mwrf32.dll", EntryPoint:="rf_request", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_request(ByVal icdev As Integer, ByVal mode As Integer, ByRef tagtype As UInt16) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_request_std", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_request_std(ByVal icdev As Integer, ByVal mode As Integer, ByRef tagtype As UInt16) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_anticoll", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_anticoll(ByVal icdev As Integer, ByVal bcnt As Integer, ByRef snr As UInt32) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_select", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_select(ByVal icdev As Integer, ByVal snr As UInt32, ByRef _size As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_authentication", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_authentication(ByVal icdev As Integer, ByVal mode As Integer, ByVal secnr As Integer) As Int16
    End Function

    '说明：     返回设备当前状态
    <DllImport("mwrf32.dll", EntryPoint:="rf_reset", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_reset(icdev As Integer, msec As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_authentication_2", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_authentication_2(ByVal icdev As Integer, ByVal mode As Integer, ByVal keynr As Integer, ByVal blocknr As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_read", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_read(ByVal icdev As Integer, ByVal blocknr As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal databuff() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_read_hex", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_read_hex(ByVal icdev As Integer, ByVal blocknr As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal databuff() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_write", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_write(ByVal icdev As Integer, ByVal blocknr As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal databuff() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_write_hex", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_write_hex(ByVal icdev As Integer, ByVal blocknr As Integer, <MarshalAs(UnmanagedType.LPArray)> ByVal databuff() As Byte) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_halt", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_halt(ByVal icdev As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_initval", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_initval(ByVal icdev As Integer, ByVal blocknr As Integer, ByVal val As UInt32) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_readval", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_readval(ByVal icdev As Integer, ByVal blocknr As Integer, ByRef val As UInt32) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_increment", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_increment(ByVal icdev As Integer, ByVal blocknr As Integer, ByVal val As UInt32) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_decrement", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_decrement(ByVal icdev As Integer, ByVal blocknr As Integer, ByVal val As UInt32) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_restore", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_restore(ByVal icdev As Integer, ByVal blocknr As Integer) As Int16
    End Function

    <DllImport("mwrf32.dll", EntryPoint:="rf_transfer", SetLastError:=True, CharSet:=CharSet.Auto, ExactSpelling:=False, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function rf_transfer(ByVal icdev As Integer, ByVal blocknr As Integer) As Int16
    End Function
End Class
